## Dependencies
The code was written in Java 1.8 and should be compatible with Java 1.6 and 1.7.
No non-default packages were used.


## Basic Idea and Runtime Analysis
The graph itself is represented by a 2D hashmap. A min-heap is also maintained to deal with the graph pruning step
Let e be the maximum number of edges in the graph at any point. Let n be the maximum number of nodes at any point. Let N be the total number of lines of data.
There are roughly 3 graph operations:
1. add_edge. O(1)
2. update(pruning). O(eloge) to extract edges from the heap until we're at the acceptable timeframe. 
3. get_median_degree. O(n) for computing the number of values of each node in the 2d hashmap(this is effectively the degree). O(nlog(n)) for computing median by sorting. 
Which means that the algorithm is O(max(eloge,nlogn)) for processing one line.
Note: If a min-heap wasn't used the second step would have been O(e^2) in order to search for edges to prune, making the algorithm O(max(e^2,nlogn)) for processing one line.

The entire algorihm is O(N*max(eloge,nlogn))


