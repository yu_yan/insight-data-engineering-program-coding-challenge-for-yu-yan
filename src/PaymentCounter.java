
/**
 * @author YY
 * A PaymentCounter is a counter where the addPayment and removePayment methods reflect the 
 * undirectedness of the graph
 */
public class PaymentCounter extends Counter {

	public void addPayment(Payment p) {
		if (this.get(p.target, p.actor) < p.time) {
			// only add when the current time is less than the new time
			this.add(p.actor, p.target, p.time);//these 2 operations take out the order
			this.add(p.target, p.actor, p.time);
		}
	}

	public void removePayment(Payment p) {
		if (this.get(p.actor, p.target) == p.time) {
			this.remove(p.actor, p.target);
			this.remove(p.target, p.actor);
		}
	}
}
