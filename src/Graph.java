import java.util.ArrayList;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.PriorityQueue;

/**
 * @author YY
 * The graph class is the meat of the algorithm
 */
public class Graph {
	public PaymentCounter pCounter; //graph implemented as an adjacency matrix via the 2D hashmap
	public PriorityQueue<Payment> paymentsPQ; //the priority queue helps with eliminating edges outside the 60 second window

	public Graph() {
		this.pCounter = new PaymentCounter();
		this.paymentsPQ = new PriorityQueue<Payment>();
	}

	public void add_edge(Payment p) {
		// each add consists of both adding to the adjacency matrix and also to the priority queue
		// the space taken by the priority queue is upper bounded by the adjacency matrix and
		// thus does not add to the space complexity
		this.pCounter.addPayment(p);
		this.paymentsPQ.add(p);
	}

	public void update(Long newMaxTime) {
		while (this.paymentsPQ.peek() != null && newMaxTime - this.paymentsPQ.peek().time >= 60) {
			// elements are extracted from the priority queue until we're within the 60 second timeframe
			Payment badPayment = this.paymentsPQ.poll();
			this.pCounter.removePayment(badPayment);
		}
	}

	public double get_median_degree() {
		
		ArrayList<Integer> numTargets = new ArrayList<Integer>();
		// The neat part of using hashmaps is this: the degree of a node is captured from the 
		// number of elements in the respective key in an O(1) operation.
		Collection<HashMap<String, Long>> valueMap = this.pCounter.map.values();
		for (HashMap<String, Long> kMap : valueMap) {
			numTargets.add(kMap.size());
		}
		return get_median(numTargets);
	}

	public double get_median(ArrayList<Integer> numTargets) {
		// A generic median getting function
		// The list is sorted before the middle(or mean of the 2 middles) are returned
		Collections.sort(numTargets);
		if (numTargets.size() % 2 == 1) {
			return (double) numTargets.get((numTargets.size() - 1) / 2);
		} else {
			return (double) (numTargets.get((numTargets.size()) / 2) + numTargets.get((numTargets.size()) / 2 - 1))
					/ 2.0;
		}
	}
}
