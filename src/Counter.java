
import java.util.HashMap;

/**
 * @author YY
 * A counter is a modified hashmap with some methods added in to simplify the syntax
 * It is the parent class of PaymentCounter
 */
public class Counter {
	public HashMap<String, HashMap<String, Long>> map;

	public Counter() {
		this.map = new HashMap<String, HashMap<String, Long>>();
	}

	public void add(String k1, String k2, long value) {
		if (this.map.get(k1) == null) {
			this.map.put(k1, new HashMap<String, Long>());
		}

		this.map.get(k1).put(k2, value);

	}

	public long get(String k1, String k2) {
		if (this.map.get(k1) != null && this.map.get(k1).get(k2) != null) {
			return this.map.get(k1).get(k2);
		} else {
			return Long.MIN_VALUE;
		}
	}

	public void remove(String k1, String k2) {
		this.map.get(k1).remove(k2);
		if (this.map.get(k1).isEmpty()) {
			this.map.remove(k1);
		}
	}
}
