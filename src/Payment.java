import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EmptyStackException;

/**
 * @author YY
 * A payment class corresponds to a line of the data. 
 */
public class Payment implements Comparable {
	public long time;
	public String target;
	public String actor;

	public void parseLine(String line) throws ParseException {
		// given a string containing the data, parse it and assign the attributes accordingly.
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
		int created_time_index = line.lastIndexOf("\"created_time\": \"");
		int end_of_time_index = line.indexOf("\"", created_time_index + 17);
		String timeString = line.substring(created_time_index + 17, end_of_time_index);
		int target_index = line.lastIndexOf("\"target\": \"");
		int end_of_target_index = line.indexOf("\"", target_index + 11);
		String targetString = line.substring(target_index + 11, end_of_target_index);

		int actor_index = line.lastIndexOf("\"actor\": \"");
		int end_of_actor_index = line.indexOf("\"", actor_index + 10);
		String actorString = line.substring(actor_index + 10, end_of_actor_index);

		if (actor_index == -1 || target_index == -1 || created_time_index == -1) {
			// make it an error if the actor, target or created_time fields are
			// not found
			throw new EmptyStackException();
		}

		Date parsedDate = dateFormat.parse(timeString);
		this.time = parsedDate.getTime() / 1000;
		this.target = targetString;
		this.actor = actorString;

	}

	public String toString() {
		return String.valueOf(this.time);
	}

	@Override
	public int compareTo(Object o) {
		return (int) (this.time - ((Payment) o).time);
	}

}
