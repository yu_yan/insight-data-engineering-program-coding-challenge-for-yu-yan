import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.ParseException;
import java.util.*;

/**
 * @author YY
 * The main class for computing rolling medians
 */
public class rollingMedian {

	public static void main(String[] args) throws IOException, ParseException {
		String inputFile = "venmo_input/venmo-trans.txt";
		BufferedReader bf = new BufferedReader(new FileReader(inputFile));

		ArrayList<Double> medians = new ArrayList<Double>();

		long maxTime = Long.MIN_VALUE;
		Graph G = new Graph();

		String line;
		while ((line = bf.readLine()) != null) {

			Payment currPayment = new Payment();
			try {
				currPayment.parseLine(line);

			} catch (Exception e) {
				//if there were any issues in parsing the line, skip the current iteration
				continue;
			}
			if (currPayment.time > maxTime) {
				maxTime = currPayment.time;

				G.add_edge(currPayment);
				G.update(maxTime); //if maxTime is updated, need to update graph
			} else {
				if (maxTime - currPayment.time < 60) {
					// if maxTime isn't changed, need to check that the entry is within the acceptable time frame
					G.add_edge(currPayment);
				}
			}
			medians.add(G.get_median_degree());
		}
		bf.close();

		FileWriter writer = new FileWriter("venmo_output/output.txt");
		for (double m : medians) {
			writer.write(String.format("%.2f", m));
			writer.write("\n");
		}
		writer.close();
	}

}
